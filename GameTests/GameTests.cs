﻿using NUnit.Framework;
using Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Tests
{
    [TestFixture()]
    public class GameTests
    {
        private Game game;
        [SetUp]
        public void SetUp()
        {
            game = new Game();
            騎士類別.人數 = 0;
            小嘍囉類別.人數 = 0;
        }

        [Test()]
        // Tset Case#1：
        //    ACT：建立 Game 物件
        // Assert：(1)產生1隻小嘍囉
        public void 建立Game物件()
        {
            // arrange

            // act

            // assert
            Assert.AreEqual(0, 小嘍囉類別.人數);
        }

        [Test()]
        // Tset Case#2：
        //   ACT：新增5隻小嘍囉
        //Assert：(1)產生5隻小嘍囉
        public void NewGame_產生5隻小嘍囉()
        {
            // arrange

            // act
            game.新增小嘍囉(5);

            // assert
            Assert.AreEqual(5, 小嘍囉類別.人數);
        }

        [Test()]
        // Tset Case#3：
        //   ACT：新增1隻血量為50的小嘍囉
        //Assert：(1) 血量是50的小嘍囉
        public void 新增1隻血量為50的小嘍囉()
        {
            // arrange

            // act
            game.新增小嘍囉(1);

            // assert
            Assert.AreEqual(1, 小嘍囉類別.人數);
            Assert.AreEqual(50, game.小嘍囉(1).血量);
        }

        [Test()]
        // Tset Case#4：
        //   ACT：新增1隻血量為40的騎士
        //Assert：(1) 血量是50的小嘍囉
        public void 新增1隻血量為40的騎士()
        {
            // arrange

            // act
            game.新增騎士(1);

            // assert
            Assert.AreEqual(1, 騎士類別.人數);
            Assert.AreEqual(40, game.騎士(1).血量);
        }

        [Test()]
        // Tset Case#5：
        //   ACT：攻擊力10的騎士對第1隻小嘍囉發動1次攻擊
        //Assert：(1) 小嘍囉的血量為40
        public void 攻擊力10的騎士對第1隻小嘍囉發動1次攻擊()
        {
            // arrange

            // act
            game.新增小嘍囉(1);
            game.新增騎士(1);
            ((騎士類別)game.騎士(1)).普通攻擊((小嘍囉類別)game.小嘍囉(1));

            // assert
            Assert.AreEqual(1, 小嘍囉類別.人數);
            Assert.AreEqual(40, game.小嘍囉(1).血量);
        }

        [Test()]
        [TestCase(10)]
        // Tset Case#6：
        //   ACT：攻擊力10的騎士對n隻小嘍囉發動1次攻擊
        //Assert：(1) n隻小嘍囉的血量皆為40
        public void 攻擊力10的騎士對n隻小嘍囉發動1次攻擊(int 小嘍囉的數量)
        {
            // arrange

            // act
            game.新增小嘍囉(小嘍囉的數量);
            game.新增騎士(1);
            for (int i = 1; i <= 小嘍囉類別.人數; i++)
            {
                game.騎士(1).普通攻擊(game.小嘍囉(i));
            }

            // assert
            for (int i = 1; i <= 小嘍囉類別.人數; i++)
                Assert.AreEqual(40, game.小嘍囉(i).血量);
        }

        [Test()]
        // Tset Case#7：
        //   ACT：攻擊力10的小嘍囉對騎士發動1次攻擊
        //Assert：(1) 騎士的血量為30
        public void 攻擊力10的小嘍囉對騎士發動1次攻擊()
        {
            // arrange

            // act
            game.新增小嘍囉(1);
            game.新增騎士(1);
            game.小嘍囉(1).普通攻擊(game.騎士(1));

            // assert
            Assert.AreEqual(30, game.騎士(1).血量);
        }

        [TestCase(9)]
        // Tset Case#8：
        //   ACT：攻擊力10的小嘍囉對n隻騎士發動3次攻擊
        //Assert：(1) 所有騎士的血量皆為10
        public void 攻擊力10的小嘍囉對n隻騎士發動3次攻擊(int 騎士的數量)
        {
            // arrange

            // act
            game.新增小嘍囉(1);
            game.新增騎士(騎士的數量);
            for (int i = 1; i <= 騎士類別.人數; i++)
                for (int j = 0; j < 3; j++)
                {
                    game.小嘍囉(1).普通攻擊(game.騎士(i));
                }

            // assert
            for (int i = 1; i <= 騎士類別.人數; i++)
                Assert.AreEqual(10, game.騎士(i).血量);
        }

        [Test()]
        // Tset Case#9：
        //   ACT：新增0隻小嘍囉
        //Assert：(1) Exception
        public void 新增0隻小嘍囉()
        {
            // arrange

            // act
            var ex = Assert.Catch(() => game.新增小嘍囉(0));

            // assert
            Assert.AreEqual("新增小嘍囉的數量錯誤", ex.Message);
        }

        [Test()]
        // Tset Case#10：
        //   ACT：新增0隻騎士
        //Assert：(1) Exception
        public void 新增0隻騎士()
        {
            // arrange

            // act
            var ex = Assert.Catch(() => game.新增騎士(0));

            // assert
            Assert.AreEqual("新增騎士的數量錯誤", ex.Message);
        }

        [Test()]
        // Tset Case#11：
        //   ACT：分別新增5隻小嘍囉與300隻小嘍囉
        //Assert：(1) Exception
        public void 分別新增5隻小嘍囉與300隻小嘍囉()
        {
            // arrange

            // act
            game.新增小嘍囉(5);
            game.新增小嘍囉(300);

            // assert
            Assert.AreEqual(305, 小嘍囉類別.人數);
        }

        [Test()]
        // Tset Case#12：
        //   ACT：分別新增5隻騎士與300隻騎士
        //Assert：(1) Exception
        public void 分別新增5隻騎士與300隻騎士()
        {
            // arrange

            // act
            game.新增騎士(5);
            game.新增騎士(300);

            // assert
            Assert.AreEqual(305, 騎士類別.人數);
        }
    }
}