﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    abstract public class 人物的抽象類別
    {
        //Field
        private int _血量;

        // Property
        public int 血量
        {
            get { return _血量; }
            set { this._血量 = value; }
        }
    }

    public class 簡易工廠類別
    {
        // Field
        public static 人物的抽象類別 建立角色(String 要產生的角色)
        {
            if (String.Equals(要產生的角色, "騎士"))
            {
                return new 騎士類別();
            }
            else if (String.Equals(要產生的角色, "小嘍囉"))
            {
                return new 小嘍囉類別();
            }
            else
                throw new NotImplementedException();
        }

        // Constructor

    }
    public class 騎士類別 : 人物的抽象類別
    {
        // Field
        static private int _人數 = 0;

        // Constructor
        public 騎士類別()
        {
            血量 = 40;
            _人數 += 1;
        }

        // Property
        static public int 人數
        {
            get { return _人數; }
            set { _人數 = value; }
        }

        // Method
        public void 普通攻擊(小嘍囉類別 被攻擊的對象)
        {
            if (被攻擊的對象.血量 > 0)
            {
                被攻擊的對象.血量 -= 10;
            }
        }
    }

    public class 小嘍囉類別 : 人物的抽象類別
    {
        //Field
        static private int _人數 = 0;

        // Constructor
        public 小嘍囉類別()
        {
            血量 = 50;
            _人數 += 1;
        }

        // Property
        static public int 人數
        {
            get { return _人數; }
            set { _人數 = value; }
        }

        // Method
        public void 普通攻擊(騎士類別 被攻擊的對象)
        {
            if (被攻擊的對象.血量 > 0)
            {
                被攻擊的對象.血量 -= 10;
            }
        }
    }
    public class Game
    {
        // Field
        private 小嘍囉類別[] _小嘍囉;
        private 騎士類別[] _騎士;

        // Constructor

        // Property

        // Method
        public int 新增角色(String 角色, int 數量)
        {
            try
            {
                if (數量 <= 0)
                    throw new Exception($"新增{角色}的數量錯誤");

                int startIndex = 0;
                if (String.Equals(角色, "騎士"))
                {
                    startIndex = 騎士類別.人數;
                    if (_騎士 == null)
                        _騎士 = new 騎士類別[數量];
                    else
                        Array.Resize(ref _騎士, _騎士.Length + 數量);
                    for (int i = 0; i < 數量; i++)
                    {
                        _騎士[startIndex + i] = (騎士類別)簡易工廠類別.建立角色(角色);
                    }
                    return _騎士.Length;
                }
                else if (String.Equals(角色, "小嘍囉"))
                {
                    startIndex = 小嘍囉類別.人數;
                    if (_小嘍囉 == null)
                        _小嘍囉 = new 小嘍囉類別[數量];
                    else
                        Array.Resize(ref _小嘍囉, _小嘍囉.Length + 數量);
                    for (int i = 0; i < 數量; i++)
                    {
                        _小嘍囉[startIndex + i] = (小嘍囉類別)簡易工廠類別.建立角色(角色);
                    }
                    return _小嘍囉.Length;
                }
                else
                    throw new ArgumentException($"{角色}是不存在的角色，所以無法建立");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
        public void 新增小嘍囉(int 數量)
        {
            新增角色("小嘍囉", 數量);
        }
        public 小嘍囉類別 小嘍囉(int index)
        {
            return _小嘍囉[index - 1];
        }
        public void 新增騎士(int 數量)
        {
            新增角色("騎士", 數量);
        }
        public 騎士類別 騎士(int index)
        {
            return _騎士[index - 1];
        }

        static void Main(string[] args)
        {
        }
    }
}
